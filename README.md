# Halo

Perkenalkan saya **Muhamad Aliefian Rahmatulloh** mahasiswa Universitas Multimedia Nusantara 2017 program studi teknik komputer. Saya saat ini sedang mempersiapkan untuk memasuki tahun ajaran baru 2020/2021 dan masuk semester 7. 

# Projek

Saya juga memiliki beberapa projek yang telah buat selama kuliah ini, yaitu :

 - [tokolapak999.000webhostapp.com](http://tokolapak999.000webhostapp.com/)
 - [twitchat.000webhostapp.com](http://twitchat.000webhostapp.com/)

# Kontak

Email : aliefian000@gmail.com
Whatsapp : 08982571010