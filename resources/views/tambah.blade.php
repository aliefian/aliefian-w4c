<!-- Menghubungkan dengan view template master -->
@extends('master')
 
<!-- isi bagian konten -->
@section('konten')
    <div class="d-flex justify-content-center">
		<h3>Tambah Data</h3>
	</div>
        
    <br/>
    
    <div class="d-flex justify-content-center">
        <form action="/home/store" method="post">
            {{ csrf_field() }}
            
            <label class="form-group">Nama Sampah</label>
            <input class="form-control" type="text" name="nama_sampah" required="required"> <br/>

            <label class="form-group">Jenis Sampah</label>
            <select name="jenis_sampah" class="form-control" ><option>Plastik<option>Logam<option>Kertas</select><br/>
               
            <a href="/" class="btn btn-secondary btn-lg"> Batal</a>
            <input type="submit" value="Simpan Data" class="btn btn-lg btn-primary">
        </form>
    </div>
@endsection