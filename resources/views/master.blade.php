<!DOCTYPE html>
<html>
<head>
	<title>Web Engineer Challenge</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>

   <div id="wrapper">

        <!-- Sidebar menu -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
				<li>
					<div class="container">
 						<div class="row bg-primary">
    						<div class="col-sm">
								
									<a href="/"><i href="/" class="fa fa-github text-white " aria-hidden="true"></i></a>
    							
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="container">
 						<div class="row bg-black text-white">
    						<div class="col-sm">
      						Navigasi
    						</div>
						</div>
					</div>
				</li>
				<li>
					<div class="container">
						<div class="row">
							<div class="col-sm">
								<div class="dropdown">
									<button class="btn btn-black dropdown-toggle text-white" data-toggle="dropdown">Sampah</button>
									<div class="dropdown-menu">
										<ul class="collapse-styled">
											<li><a href="/home/tambah">Tambah Sampah</a></li>
											<li><a href="/">Semua Sampah</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>


            </ul>
        </div>

        <!-- bagian dari halamana konten -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">	
						<button href="#menu-toggle" class="btn btn-default" id="menu-toggle">
							<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
						</button>

						<!-- bagian konten dari tiap page -->
						@yield('konten')
                    </div>
                </div>
            </div>
        </div>
   
		<footer>	
		<div class="d-flex justify-content-center">
			<p>&copy; <a target="_blank" href="https://github.com/aliefian9">ALIEFIAN</a> 2020</p>
		</div>
		</footer>
    </div>
	
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>