<!-- Menghubungkan dengan view template master -->
@extends('master')
  
<!-- isi bagian konten -->
@section('konten')
 
	<div class="container">
		<div class="card">
			<div class="card-body">
				<div class="float-right">
					<a href="/home/tambah" class="btn btn-primary"> + Tambah</a>
				</div>
				<br/>
				<br/>

				<table class="table table-active">
					<tr>
						<th>#</th>
						<th>Nama Sampah</th>
						<th>Jenis Sampah</th>
						<th>Aksi</th>
					</tr>
				</table>
				
				<table class="table">
					<tr class="table-light">
						<td></td>
						<td>
							<form action="/home/carinama" method="GET" class="form-inline">
								<input class="form-control" type="text" name="sampah" placeholder="Cari" value="{{ old('cari') }}">
							</form>						
						</td>
						<td>
							<form action="/home/carijenis" method="GET" class="form-inline">
								<input class="form-control" type="text" name="jenis" placeholder="Cari" value="{{ old('cari') }}">
							</form>
						</td>
						<td></td>
					</tr>
					@foreach($sampah as $smp => $s)
					<tr>
						<td>{{ $smp +1 }}</td>
						<td>{{ $s->nama_sampah }}</td>
						<td>{{ $s->jenis_sampah }}</td>
						<td>
							<a class="btn btn-danger delete-user" href="/home/hapus/{{ $s->id_sampah }}">Hapus</a>
						</td>
					</tr>
					@endforeach
				</table>

				Menampilkan {{ $sampah->perPage() }} dari {{ $sampah->total() }} <br/><br/><br/>
 
				{{ $sampah->links() }}
			</div>
		</div>
	</div>

@endsection